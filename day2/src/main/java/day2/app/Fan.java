package day2.app;

public class Fan {
	private String location;
	private String size;
	private String status;
	private int bladeCount;

	public Fan(String location, String size, String status, int bladeCount) {
		super();
		this.location = location;
		this.size = size;
		this.status = status;
		this.bladeCount = bladeCount;
	}

	public int getBladeCount() {
		return bladeCount;
	}

	public String getLocation() {
		return location;
	}

	public String getSize() {
		return size;
	}

	public String getStatus() {
		return status;
	}
}
