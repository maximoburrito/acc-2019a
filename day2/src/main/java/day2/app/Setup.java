package day2.app;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

@WebListener
public class Setup implements ServletContextListener, ServletRequestListener {
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("====== STOPPING day2");
	}

	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("====== STARTING day2");
		
		// initialize application components
		sce.getServletContext().setAttribute("fanManager", new FanManager());
	}
	
	public void requestInitialized(ServletRequestEvent sre) {
		if (sre.getServletRequest() instanceof HttpServletRequest) {
			HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
			System.out.println("*" + request.getMethod() + " " + request.getRequestURI());
		}
	}
}
