package day2.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FanManager {
	private List<Fan> allFans;

	public FanManager() {
		Fan fan1 = new Fan("Room 301.9", "medium", "running", 4);
		Fan fan2 = new Fan("Room 404", "large", "Not Found", 6);
		Fan fan3 = new Fan("Room 200", "medium", "OK", 4);
		this.allFans = new ArrayList<Fan>(Arrays.asList(fan1, fan2, fan3));
	}

	public List<Fan> getFans() {
		return allFans;
	}

	public void addRandomFan() {
		allFans.add(new Fan("fan#" + System.currentTimeMillis(), "medium", "unknown", 4));
	}

}
