package day2.app;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/new")
public class NewFanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private FanManager fanManager() {
		return (FanManager) getServletContext().getAttribute("fanManager");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// reminder - this was an example of what NOT to do. This should be a POST w/ redirect
		fanManager().addRandomFan();
		response.getWriter().append("fan count " + fanManager().getFans().size());
	}
}
