package day2.app;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private FanManager fanManager() {
		return (FanManager) getServletContext().getAttribute("fanManager");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {				
		request.setAttribute("fans", fanManager().getFans());		
		getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}
}
