package day8.crud;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface GameRespository 
  extends CrudRepository<HighScore, Long>
{
   List<HighScore> findByGameType(GameType type);
}
  