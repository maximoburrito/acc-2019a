package day8.crud;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

enum GameType {
	SHOOTER,
	PLATFORM,
	SPORTS,  
	PUZZLE,
	OTHER
}

@Entity
public class HighScore {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	@NotEmpty
	String gameName;
	
	@NotNull(message = "score is required")
	Long score;
	
	@NotEmpty
	String notes;
	
	@NotNull
	GameType gameType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public GameType getGameType() {
		return gameType;
	}

	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	@Override
	public String toString() {
		return "HighScore [id=" + id + ", gameName=" + gameName + ", score=" + score + ", notes=" + notes
				+ ", gameType=" + gameType + "]";
	}

}

