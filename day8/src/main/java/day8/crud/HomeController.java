package day8.crud;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
	GameRespository repository;

	@GetMapping
	public String home(Model model) {
		model.addAttribute("scores", repository.findAll());
		return "home";
	}

	@GetMapping("/type/{gameType}")
	public String typeView(Model model, @PathVariable GameType gameType) {
		model.addAttribute("scores", repository.findByGameType(gameType));
		return "home";
	}
	
	@GetMapping("/new")
	public String newScore(Model model) {
		HighScore score = new HighScore();
		score.setGameType(GameType.OTHER);
		model.addAttribute("highScore", score);
		return "new";
	}

	@PostMapping("/new")
	public String saveScore(@Valid HighScore highscore, Errors errors) {
		if (errors.hasErrors()) {
			return "new";
		} else {
			repository.save(highscore);
			return "redirect:/";
		}
	}

	@GetMapping("/edit/{id}")
	public String editScore(@PathVariable Long id, Model model) {
		Optional<HighScore> optionalScore = 
				repository.findById(id);
		if (optionalScore.isPresent()) {
			model.addAttribute("highScore", optionalScore.get());
			return "edit";
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PostMapping("/edit/{id}")
	public String updateScore(@PathVariable Long id, 
			                  @Valid HighScore highscore, 
			                  Errors errors) {
		if (errors.hasErrors()) {
			return "edit";
		} else {
			repository.save(highscore);
			return "redirect:/";
		}
	}

	@PostMapping("/delete/{id}")
	public String updateScore(@PathVariable Long id) {
		Optional<HighScore> optionalScore = 
				repository.findById(id);
		if (optionalScore.isPresent()) {
		    repository.delete(optionalScore.get());
		    return "redirect:/";
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
}
