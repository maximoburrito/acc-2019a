package day1.demo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/queso")
public class QuesoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // HTML demonstrating XSS/injection vulnerability
		request.setAttribute("message", "<b>10%</b> discount, use code JAVA");
		getServletContext().getRequestDispatcher("/WEB-INF/queso.jsp")
		                   .forward(request, response);
	}
}
