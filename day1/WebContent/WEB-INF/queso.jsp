<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>Hello, world!</title>
</head>

<body>

	<div class="container">

		<div class="row">
			<div class="col">
				<div class="card" style="width: 18rem;">
					<img src="/day1/pages/queso.jpg" height="200px"
						class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">Basic Queso</h5>
						<p class="card-text">20 GOTO STOMACH!</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card" style="width: 18rem;">
					<img src="/day1/pages/queso2.jpg" height="200px"
						class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">Spinach Queso</h5>
						<p class="card-text">Queso 2.0, for those special occasions.</p>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="card" style="width: 18rem;">
					<img src="/day1/pages/queso3.jpg" height="200px"
						class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">Hatch Green Chile</h5>
						<p class="card-text">Straight from New Mexico.</p>
					</div>
				</div>
			</div>
		</div>

		<h2>Price List</h2>

		<div class="row">


			<table class="table table-striped">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Price</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Basic Queso</td>
						<td>$4.99</td>
					</tr>
					<tr>
						<th scope="row">2</th>
						<td>Spinach Queso</td>
						<td>$5.99</td>
					</tr>
					<tr>
						<th scope="row">3</th>
						<td>Hatch Queso</td>
						<td>$5.99</td>
					</tr>
				</tbody>
			</table>


		</div>
		<div class="alert alert-danger" role="alert">${message}</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>
