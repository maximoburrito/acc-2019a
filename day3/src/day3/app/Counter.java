package day3.app;

public class Counter {
	private int count = 0;
	
	public void plusOne( ) {
		this.count++;
	}
	
	public int getCount() {
		return count;
	}
}
