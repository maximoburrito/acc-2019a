package day3.app;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class Setup implements ServletContextListener, HttpSessionListener {
    public Setup() {    
    }

    public void sessionCreated(HttpSessionEvent se)  { 
    	System.out.println("++ NEW SESSION " + se.getSession().getId());
    	se.getSession().setAttribute("counter", new Counter());
    }

    public void sessionDestroyed(HttpSessionEvent se)  { 
    	System.out.println("-- END SESSION " + se.getSession().getId());
    }

    public void contextInitialized(ServletContextEvent sce)  { 
    	System.out.println("=== DAY3");
    	sce.getServletContext().setAttribute("counter", new Counter());
    }
}
