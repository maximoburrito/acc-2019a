package day3.app;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/counter")
public class CounterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Counter globalCounter = (Counter) getServletContext().getAttribute("counter");
		globalCounter.plusOne();
			
		Counter sessionCounter = (Counter) request.getSession().getAttribute("counter");
		sessionCounter.plusOne();
		
		request.setAttribute("count", globalCounter.getCount());
		request.setAttribute("myCount", sessionCounter.getCount());
		
		request.setAttribute("sessionId", request.getSession().getId());
	
		getServletContext().getRequestDispatcher("/WEB-INF/count.jsp").forward(request, response);
	}
}
