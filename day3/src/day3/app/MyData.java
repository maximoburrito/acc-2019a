package day3.app;

import java.util.Random;

public class MyData {
	private int daysUntilChristmas = 125;
	private MyOtherData otherData = new MyOtherData();
	
	public String getName() {
		return "randomname1234";
	}
	
	public int getNumberOfPeople() {
		return (new Random()).nextInt(10);
	}

	public int getDaysUntilChristmas() {
		return this.daysUntilChristmas;
	}

	public MyOtherData getOtherData() {
		return otherData;
	}
	

}
