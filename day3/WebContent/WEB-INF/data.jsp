<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>bean property testing</title>
</head>
<body>

The data is ${data.name} and ${data.numberOfPeople}.

<br />

Days until Christmas: ${data.daysUntilChristmas}.

<br />

Other data: ${data.otherData.color}


</body>
</html>