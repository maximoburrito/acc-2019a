package day5.maze;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/print")
@SessionAttributes("printers")
public class PrinterController {
	@GetMapping
	public String printerPage(Model model) {
		model.addAttribute("printer", new Printer());		
		return "printer";
	}

	@PostMapping
	public String savePrinter(@ModelAttribute("printer") Printer printer, 
			                  @ModelAttribute("printers") List<Printer> printers) {
		printers.add(printer);
		return "redirect:/print";
	}

	@ModelAttribute("printers")
	List<Printer> initialPrinters() {
	   return new ArrayList<Printer>();
	}
}
