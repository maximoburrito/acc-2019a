package day5.maze;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ping")
public class PingPong {

	@GetMapping
	@ResponseBody
	public String ping() {
		return "pong";
	}

	@GetMapping("/{text}")
	@ResponseBody
	public String pong(@PathVariable("text") String message, 
			           @RequestParam(value = "n", defaultValue = "1") int n) {
		String response = IntStream.range(0, n)
				                   .mapToObj(i -> message + i)
				                   .collect(Collectors.joining(","));

		return response;
	}

}
