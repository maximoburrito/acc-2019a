package day5.maze;

public class MazeGame {
	private Tile maze[][];
	private int mazeHeight = 10;
	private int mazeWidth = 20;
	private int playerX = 1;
	private int playerY = 1;
	private int steps = 0;

	public enum Tile {
		EMPTY, WALL, PLAYER, GOAL
	}

	public enum Direction {
		UP, DOWN, LEFT, RIGHT
	}

	public MazeGame(int height, int width) {
		this.mazeHeight = height;
		this.mazeWidth = width;

		this.maze = new Tile[mazeHeight][mazeWidth];
		for (int x = 0; x < mazeHeight; x++) {
			for (int y = 0; y < mazeWidth; y++) {
				maze[x][y] = Tile.EMPTY;
			}
		}
	}

	public int getMazeHeight() {
		return mazeHeight;
	}

	public int getMazeWidth() {
		return mazeWidth;
	}

	public int getSteps() {
		return this.steps;
	}

	public boolean atGoal() {
		return this.maze[playerY][playerX] == Tile.GOAL;
	}

	public Tile tileAt(int x, int y) {
		if (x == this.playerX && y == this.playerY) {
			return Tile.PLAYER;
		} else {
			return this.maze[y][x];
		}
	}

	public void setTileAt(int x, int y, Tile tile) {
		this.maze[y][x] = tile;
	}

	private void moveTo(int x, int y) {
		if (x >= 0 && x < mazeWidth && y >= 0 && y < mazeHeight && this.maze[y][x] != Tile.WALL) {
			this.playerX = x;
			this.playerY = y;
			this.steps++;
		}
	}

	public void move(Direction direction) {
		switch (direction) {
		case UP: {
			moveTo(playerX, playerY - 1);
			break;
		}
		case DOWN: {
			moveTo(playerX, playerY + 1);
			break;
		}
		case LEFT: {
			moveTo(playerX - 1, playerY);
			break;
		}
		case RIGHT: {
			moveTo(playerX + 1, playerY);
			break;
		}
		}

	}

}
