package day5.maze;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/wired")
public class WiredController {
//	@Bean("secret1") 
//	SecretMessage secret1() {
//		return new SecretMessage("this is secret one");
//	}
//	
//	@Bean("secret2") 
//	SecretMessage secret2() {
//		return new SecretMessage("2nd+S3CR3T");
//	}
	
	//@Autowired
	SecretMessage secret; 
	
	@GetMapping
	public String thePage(Model model) {
		model.addAttribute("number", 42);
		model.addAttribute("mySecret", secret);
		
		System.out.println("MODEL: " + model.asMap());
		System.out.println("SHHH!!! " + secret.getMessage() + " : " + secret);

		return "wired";
	}

}
