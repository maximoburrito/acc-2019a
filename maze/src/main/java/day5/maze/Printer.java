package day5.maze;

public class Printer {
	private String brand;
	private String serialNumber;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public void setSecret(String secret) {
		System.out.println("THE SECRET WAS SET TO " + secret + " --- WHOOPS!");
	}
}
