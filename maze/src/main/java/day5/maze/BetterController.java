package day5.maze;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("/better")
@SessionAttributes("betterSecret")
public class BetterController {
	@ModelAttribute("betterSecret")
	public SecretMessage makeBetterSecret() {
		return new SecretMessage("this.secret.is.the.best");
	}
		
	@ModelAttribute("aNumber") 
	int awesomeNumber() {
		return 1234;
	}
	
	@GetMapping
	public String betterPage(Model model) {
		System.out.println("model is " + model.asMap());
		return "better";
	}
	
	@GetMapping("/number") 
	@ResponseBody
	public String number(@ModelAttribute("aNumber") int n) {
		return "The number is " + n;
	}
	
}
