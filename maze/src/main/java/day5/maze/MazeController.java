package day5.maze;

import java.util.stream.IntStream;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import day5.maze.MazeGame.Tile;

@Controller
@RequestMapping("/")
@SessionAttributes("maze")
public class MazeController {
	
    @ModelAttribute("maze")
	MazeGame generateMaze() {
    	MazeGame maze = new MazeGame(10,20);
    		
    	IntStream.range(0, 5).forEach(y -> maze.setTileAt(3, y, Tile.WALL));
    	IntStream.range(4, 10).forEach(y -> maze.setTileAt(12, y, Tile.WALL));
    	maze.setTileAt(15,  8, Tile.GOAL);

		return maze;	
	}

	@GetMapping
	public String showMaze(Model m, @ModelAttribute("maze") MazeGame maze) {
		if (maze.atGoal()) {
			return "gameover";
		} else {
			return "game";
		}
	}

	@PostMapping
	public String move(@RequestParam("direction") MazeGame.Direction direction,
			           @ModelAttribute("maze") MazeGame maze) {
		maze.move(direction);
		return "redirect:/";
	}
	
	@PostMapping("/newmaze")
	public String reset(HttpSession sesion, SessionStatus status) {
		status.setComplete();
		return "redirect:/";
	}

}
