package day5.maze;

import org.springframework.stereotype.Component;

//@Component
public class SecretMessage {
	private String secret;

	public SecretMessage() {
		this.secret = "DEFAULT-SECRET-POPCORN1234!";		
	}
	
	public SecretMessage(String secret) {
		this.secret = secret;
	}

	public String getMessage() {
		return this.secret;
	}
}
