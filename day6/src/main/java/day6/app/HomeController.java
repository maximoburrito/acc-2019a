package day6.app;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class HomeController {
	@Autowired
	UserInfo userInfo;

	@Autowired
	WebsiteInfo siteInfo;

	private boolean validLogin(String userName, String password) {
		return (userName.equals("fred") && password.equals("flintstone"))
				|| (userName.equals("barney") && password.equals("rubble1234"));
	}
	
	// --------------------------------------------
	
	@GetMapping
	public String home(Model model, LoginData loginForm) {	
		System.out.println("Model is " + model.asMap());
		return "home";
	}
	
	
	@PostMapping("/login")
	public String login(LoginData loginForm, RedirectAttributes redirect, Model model) {
		if (validLogin(loginForm.getUserName(), loginForm.getPassword())) {
			userInfo.loginAs(loginForm.getUserName());
			redirect.addFlashAttribute("message", "Welcome, " + loginForm.getUserName() + "!");
			return "redirect:/";

		} else {
			//redirect.addFlashAttribute("message", "invalid username or password");
			model.addAttribute("message", "try again");
			return "home";
		}
	}


	@PostMapping("/logout") 
	public String logout(RedirectAttributes redirect) {
		userInfo.logout();
		redirect.addFlashAttribute("message", "Zaijian!");	
		return "redirect:/";
	}
	
	// ------------------------------ 
	
	@GetMapping("/internal")
	public String internalPage(Model model,
			                   RedirectAttributes redirect) {
		if (!userInfo.isLoggedIn()) {
			redirect.addFlashAttribute("message", "Access denied! Please log in!");
			return "redirect:/";
		}
		
		model.addAttribute("info", siteInfo);
		return "internal";
	}
	
	@PostMapping("/internal/update") 
	public String updateInfo(@ModelAttribute("info") 
	                         @Valid WebsiteInfo info, 
	                         //RedirectAttributes redirect
	                         Errors errors) 
	{		
		if (!userInfo.isLoggedIn()) {
			//redirect.addFlashAttribute("message", "Access denied! Please log in!");
			return "redirect:/";
		}
		
		if (errors.hasErrors()) {
			return "internal";
		} else {
		   siteInfo.setWelcomeMessage(info.getWelcomeMessage());;
		   siteInfo.setPhoneNumber(info.getPhoneNumber());
		   siteInfo.setContactEmail(info.getContactEmail());
		   
		   //redirect.addFlashAttribute("message", "site information updated");
		   return "redirect:/";
		}
	}
}
