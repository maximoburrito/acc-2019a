package day6.app;

public class UserInfo {
	
	private boolean loggedIn;
	private String userName;
	
    public boolean isLoggedIn() {
		return loggedIn;
	}

	public String getUserName() {
		return userName;
	}

	
	public void logout() {
		this.loggedIn = false;
		this.userName = null;
	}
	
	public void loginAs(String userName) {
		this.loggedIn = true;
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "UserInfo [loggedIn=" + loggedIn + ", userName=" + userName + "]";
	}
}
