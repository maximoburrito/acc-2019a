package day6.app;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class WebsiteInfo {
	private String welcomeMessage;
	private String contactEmail;
	private String phoneNumber;
	
	@NotEmpty @Size(min=10, max=999)
	public String getWelcomeMessage() {
		return welcomeMessage;
	}
	public void setWelcomeMessage(String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}
	
	@Email @NotEmpty
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
		
	@NotEmpty
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString() {
		return "WebsiteInfo [welcomeMessage=" + welcomeMessage + ", contactEmail=" + contactEmail + ", phoneNumber="
				+ phoneNumber + "]";
	}
}
