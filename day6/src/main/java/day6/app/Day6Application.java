package day6.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;

@SpringBootApplication
public class Day6Application {

	public static void main(String[] args) {
		SpringApplication.run(Day6Application.class, args);
	}

	@Bean("userInfo")
	@SessionScope
	public UserInfo makeUserSession() {
		UserInfo userInfo = new UserInfo();
		userInfo.logout();
		return userInfo;
	}
	
	@Bean("siteInfo")
	@ApplicationScope
	public WebsiteInfo siteInfo() {
		WebsiteInfo info = new WebsiteInfo();
		info.setWelcomeMessage("Welcome to the site");
		info.setContactEmail("norman@example.com");
		info.setPhoneNumber("512-999-8888");
		return info;
	}
}
