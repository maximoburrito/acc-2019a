package day4.postit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

@Controller
@RequestMapping("/game")
public class PlayTheGame {
	@Autowired
	private GameWorld currentGame;

	@GetMapping
	public String showGame(Model model) {
		model.addAttribute("game", currentGame);
		return "play";
	}

	@GetMapping("/info")
	public String info(Model model) {
		model.addAttribute("message", "no further information available");
		return "minimal";
	}

	@GetMapping("/license")
	public String license(Model model) {
		model.addAttribute("message", "Who knows - ask the lawyers.");
		return "minimal";
	}
	
	@PostMapping("/reset")
	public String reset() {
		currentGame.startOver();
		return "redirect:/game";
	}
	
	@PostMapping("/hit")
	public String hitTheDonkey(Model model) {
		currentGame.hitTheDonkey();
		return "redirect:/game";
	}
	
	@PostMapping("/sticks")
	public String sticks(Model model) {
		currentGame.pickUpSticks();
		return "redirect:/game";
	}

	// ---------------------
	// configuration - we'd probably put this somewhere else,
	// but we'll keep it here for now..
	@Bean
	@SessionScope
	GameWorld currentGame() {
		return new GameWorld();
	}

}
