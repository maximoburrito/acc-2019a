package day4.postit;

import java.util.Random;

public class GameWorld {
	private int score;
	private int energy = 10;
	private String message;

	public int getScore() {
		return score;
	}

	public void startOver() {
		this.message = "You had " + this.score + " points. Can you do better this time?";
		this.score = 0;
		this.energy = 10;
	}
	
	/* returns true if we consumed an energy */
	public boolean useEnergy() {
		if (this.energy > 0) {
			this.energy--;
			return true;
		} else {
			return false;
		}
	}

	public void hitTheDonkey() {
		if (useEnergy()) {
			Random random = new Random();
			if (random.nextBoolean()) {
				this.message = "Congratulations, you got five points!";
				this.score += 5;
			} else {
				this.message = "You loose two points, animal abuser!";
				this.score -= 2;
			}
		} else {
			this.message = "You are too tired to hit anything. Get some rest!";
		}
	}

	public void pickUpSticks() {
		if (useEnergy()) {
			this.message = "Great. You have some sticks, here's a point!";
			this.score++;
		} else {
			this.message = "You don't really feel like doing that. You buckle your shoe instead.";
		}
	}

	public String getMessage() {
		return message;
	}

	public int getEnergy() {
		return energy;
	}

	public String toString() {
		return "world#" + this.hashCode();
	}
}
