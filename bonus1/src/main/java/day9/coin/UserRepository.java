package day9.coin;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{
	public Optional<User> findByLogin(String login);
}
