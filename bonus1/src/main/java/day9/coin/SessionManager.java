package day9.coin;


public class SessionManager {
	User loggedInUser = null;
	
	public boolean isLoggedIn() {
		return loggedInUser != null;
	}
	
    public User getLoggedInUser() {
		return loggedInUser;
	}
    
    public void login(User user) {
    	this.loggedInUser = user;
    }
    
    public void logout() {
    	this.loggedInUser = null;
    }

    @Override
    public String toString() {
    	if (loggedInUser!=null) {
    		return "sessionManager[" + loggedInUser.getId() + ":" + loggedInUser.getLogin() + "]";
    	} else {
    		return "sessionmanager[loggedout]";
    	}
    }
    
}
