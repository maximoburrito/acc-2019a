package day9.coin;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface CoinRepository extends CrudRepository<ChumpCoin, Long>  {
	public List<ChumpCoin> findAllByOrderByIdDesc();
	
	public List<ChumpCoin> findByOwnerId(int i);
	public List<ChumpCoin> findByOwnerName(String name);

	public List<ChumpCoin> findByOwnerIdOrderByDate(int i);
	public List<ChumpCoin> findByOwnerIdOrderById(int i);
	public List<ChumpCoin> findByOwnerIdOrderByIdDesc(int i);
	
	 
	public Page<ChumpCoin> findByOwnerIdOrderByDate(int i, Pageable pageable);

	
	public Page<ChumpCoin> findAll(Pageable pageable);
	
}
