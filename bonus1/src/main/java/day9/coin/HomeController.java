package day9.coin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	private static final int PAGE_SIZE = 5;

	@Autowired
	CoinRepository coins;
	
	@Autowired
	SessionManager sessionManager;
	
	@GetMapping("/")
	public String homePage(Model model) {	
		if (sessionManager.isLoggedIn()) {
			model.addAttribute("coins", coins.findByOwnerIdOrderByDate(sessionManager.getLoggedInUser().getId()));
			
		}
		return "home";
	}

	
	@GetMapping("/coins") public String myCoins (Model model) {
		if (!sessionManager.isLoggedIn()) {
			return "redirect:/";
		}
		
		//int ownerId = sessionManager.getLoggedInUser().getId();
		PageRequest page = PageRequest.of(0, PAGE_SIZE, Sort.by("quantity"));
		//Page<ChumpCoin> coinPage = coins.findByOwnerIdOrderByDate(ownerId, page);
		Page<ChumpCoin> coinPage = coins.findAll(page);

		model.addAttribute("coins", coinPage);
		return "coins";	
	}
	
	@GetMapping("/coins/{page}") public String myCoinsPaged (Model model,
			@PathVariable("page") int pageNumber) {
		if (!sessionManager.isLoggedIn()) {
			return "redirect:/";
		}
		
		Page<ChumpCoin> page = coins.findAll(PageRequest.of(pageNumber-1, PAGE_SIZE, Sort.by("quantity")));
		model.addAttribute("coins", page);
		return "coins";	
	}
}
