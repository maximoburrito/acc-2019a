package day9.coin;

import java.time.DayOfWeek;
import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

@Controller
@RequestMapping("/coin")
public class CoinController {
	@Autowired
	UserRepository users;

	@Autowired
	CoinRepository coins;

	@Autowired
	SessionManager sessionManager;

	private ChumpCoin defaultCoin() {
		ChumpCoin coin = new ChumpCoin();
		coin.setQuantity(1);
		coin.setDate(LocalDate.now());
		coin.setOwner(sessionManager.getLoggedInUser());
		return coin;
	}

	@GetMapping
	public String newCoin(Model model) {
		if (!sessionManager.isLoggedIn()) {
			return "redirect:/login";
		}

		model.addAttribute("coin", defaultCoin());
		return "coin";
	}

	// @Validated({ Purchase.class, Default.class}
	@PostMapping
	public String createCoin(@Valid @ModelAttribute("coin") ChumpCoin newCoin, Errors errors) {
		if (!sessionManager.isLoggedIn()) {
			return "redirect:/login";
		}

		if (newCoin.getQuantity() == 4) {
			errors.rejectValue("quantity", "unlucky", "That number is unlucky!");
		} 
		
		if (isFriday13(newCoin.getDate())) {
			errors.rejectValue("date", "unlucky", "That date is unlucky!");
		}
			
		System.out.println("****" + errors);

		if (errors.hasErrors()) {
			return "coin";
		} else {
			newCoin.setOwner(sessionManager.getLoggedInUser());
			coins.save(newCoin);
			return "redirect:/";
		}
	}

	private boolean isFriday13(LocalDate date) {
		return (date!=null) &&
			   (date.getDayOfWeek() == DayOfWeek.FRIDAY) && 
			   (date.getDayOfMonth() == 13);
	}
}
