package day9.coin;

import javax.validation.constraints.NotEmpty;

public class LoginForm {
	@NotEmpty
	String login;
	@NotEmpty
	String password;
	
	@Override
	public String toString() {
		return "LoginForm [login=" + login + ", password=" + password + "]";
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
		
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
}
