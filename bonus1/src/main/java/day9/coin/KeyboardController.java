package day9.coin;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/keyboard")
public class KeyboardController {
	@GetMapping
	public String keyboard() {
		return "keyboard";
	}

	@SuppressWarnings("null")
	@GetMapping("/fail") 
	public String shouldFail() 
	{
		User user = null;
		
		user.getName();
		return "keyboard";
		
	}
	
	@GetMapping("/fail2") 
	public String shouldFail2() 
	{
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You have no permissions");
		
	}
}
