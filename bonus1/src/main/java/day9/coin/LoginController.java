package day9.coin;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping()
public class LoginController {
	@Autowired 
	UserRepository users;
	
	@Autowired 
	SessionManager sessionManager;
	
	
	@GetMapping("/login")
	public String login(Model model) {
		if (sessionManager.isLoggedIn()) {
			return "redirect:/";
		}
		model.addAttribute("loginForm", new LoginForm());	
		return "login";
	}
	
	@PostMapping("/login")
	public String login(@Valid LoginForm form, Errors errors, Model m) {
		if (sessionManager.isLoggedIn()) {
			return "redirect:/";
		}
		
		if (!errors.hasErrors()) {
			Optional<User> optionalUser = users.findByLogin(form.getLogin());
			if (optionalUser.isPresent()) {
				User user = optionalUser.get();
				
				String hash = Hash.hash(form.getPassword(), user.getSalt());
				if (hash.equals(user.getPassword())) {
					sessionManager.login(user);			
				}
			}
		}
		
		if (sessionManager.getLoggedInUser() == null) {
			return "login";
		} else {
			return "redirect:/";
		}
	}
	
	@PostMapping("/logout")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();

		return "redirect:/";
	}
}
