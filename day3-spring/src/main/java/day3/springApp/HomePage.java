package day3.springApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomePage {
	@Autowired
	Counter localCounter;
	
	@Autowired
	Counter globalCounter;

	@GetMapping
	public String homePage(Model model, ThingManager things) {
		model.addAttribute("color", "white");
		model.addAttribute("message", "Your counters are");
		model.addAttribute("things", things.favoriteThings());
	
		localCounter.increment();
		globalCounter.increment();
		
		model.addAttribute("counter1", localCounter);
		model.addAttribute("counter2", globalCounter);
		
		return "home";
	}
}
