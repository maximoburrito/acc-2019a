package day3.springApp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/info")
public class InfoPage {
	
	@GetMapping
	public String infoPage() {
		return "info";
	}
}
