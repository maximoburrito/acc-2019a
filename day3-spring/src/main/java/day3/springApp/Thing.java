package day3.springApp;

public class Thing {
	private String name;

	public String getName() {
		return name;
	}

	public Thing(String name) {
		super();
		this.name = name;
	}

}
