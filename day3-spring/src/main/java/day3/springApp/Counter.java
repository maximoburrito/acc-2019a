package day3.springApp;

import org.springframework.stereotype.Component;

@Component
public class Counter {
	private int count = 0;
	
	public int getCount() {
		return this.count;
	}

	public void increment() {
		this.count++;
	}

}
