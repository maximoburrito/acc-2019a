package day3.springApp;

import java.util.Arrays;
import java.util.List;

public class ThingManager {
	
	public List<Thing> favoriteThings() {
		return Arrays.asList(new Thing("one"), new Thing("two"), new Thing("Ben Grimm"));	
	}
}
