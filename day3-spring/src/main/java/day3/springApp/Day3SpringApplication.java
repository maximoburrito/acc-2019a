package day3.springApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;

@SpringBootApplication
public class Day3SpringApplication {
	public static void main(String[] args) {
		System.out.println("... START");
		SpringApplication.run(Day3SpringApplication.class, args);
	}
	
	
	@Bean
	@SessionScope
	Counter localCounter() {
		return new Counter();
	}
	
	@Bean
	@ApplicationScope
	Counter globalCounter() {
		return new Counter();
	}
	
	
}