package day7.app;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MagazineSubscription {
	@Id
	@GeneratedValue
	Long id;
	String name;
	int subscriptionLength;
	String description;
	
	public MagazineSubscription( ) {	
	}
	
	public MagazineSubscription(String name, String description, int subscriptionLength, boolean active) {
		super();
		this.name = name;
		this.subscriptionLength = subscriptionLength;
		this.description = description;
		this.active = active;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSubscriptionLength() {
		return subscriptionLength;
	}
	public void setSubscriptionLength(int subscriptionLength) {
		this.subscriptionLength = subscriptionLength;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	boolean active;
	

}
