package day7.app;

import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day7Application {

	public static void main(String[] args) {
		SpringApplication.run(Day7Application.class, args);
	}


	@PostConstruct
	public void atStartup() {
		System.out.println("=============================== The system is up, yo!");
        // we could load some data here
		// loadSomeData();
	}


	@Autowired
	MagazineRepository magazineRepository;
	
	void loadSomeData() {
		IntStream.range(1, 20).forEach(
				n -> magazineRepository.save(new MagazineSubscription("Magazine#" + n, "blah blah blah", n, true)));
	}

}
