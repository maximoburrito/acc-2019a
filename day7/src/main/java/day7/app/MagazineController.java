package day7.app;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/magazines")
public class MagazineController {
	@Autowired
	MagazineRepository magazineRepository;

	@GetMapping
	public String listMagazines(Model model) {
		model.addAttribute("magazines", magazineRepository.findAll());

		return "magazines";
	}

	@GetMapping("/{magazineId}")
	public String magazineDetails(Model model, @PathVariable Long magazineId) {
		Optional<MagazineSubscription> magazine = magazineRepository.findById(magazineId);

		if (magazine.isPresent()) {
			model.addAttribute("magazine", magazine.get());
			return "magazine";
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/{magazineId}/subscribe")
	public String subscribe(@PathVariable Long magazineId) {
		Optional<MagazineSubscription> optionalMagazine = 
				magazineRepository.findById(magazineId);
		if (optionalMagazine.isPresent()) {
			MagazineSubscription magazine = optionalMagazine.get();
			magazine.setSubscriptionLength(
					magazine.getSubscriptionLength() + 12);
			magazineRepository.save(magazine);
			
			return "redirect:/magazines/" + magazineId;
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	// param example
	@GetMapping("/p") 
	public String paramExample(Model model, Long id) {
		Optional<MagazineSubscription> magazine = magazineRepository.findById(id);

		if (magazine.isPresent()) {
			model.addAttribute("magazine", magazine.get());
			return "magazine";
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	

}
