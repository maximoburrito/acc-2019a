package day7.app;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/password")
public class HashController {
	
	// https://www.owasp.org/index.php/Hashing_Java
	private static byte[] owaspHash(
			final char[] password, 
			final byte[] salt, 
			final int iterations,
			final int keyLength) {

		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public String hash(String password) {
		return Base64.getEncoder().encodeToString(owaspHash(password.toCharArray(),
		         "badsalt".getBytes(),
		         10000,
		         256));	
	}
	
	
	private String adminHash = "4hPvudbWbId3yOJxXjqQp/yrShJe+HbdZUznmdNGRUs=";

	@GetMapping
	public String hashPage(Model model, String password) {
		model.addAttribute("hashedPassword", hash(password));
		model.addAttribute("hashMatches", adminHash.equals(hash(password)));
		
		return "page";
	}

}
