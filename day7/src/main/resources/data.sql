insert into MAGAZINE_SUBSCRIPTION 
(ID, NAME, DESCRIPTION, SUBSCRIPTION_LENGTH, ACTIVE) 
values
(1, 'Better Java Coding', 'an awesome magazine for java developers', 8, true),
(2, 'Potato Monthly', 'All about potatoes ', 24, true),
(3, 'Ninja Skills', 'Tips and tricks for the ninja of all skills', 7, true);

select 1;